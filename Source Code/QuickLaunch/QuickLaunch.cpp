﻿// QuickLaunch.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "QuickLaunch.h"
#include "windowsx.h"
#include <string>
#include <vector>
#include <shellapi.h>
#pragma comment(lib, "Shell32.lib")

//using StrCpy, StrNCat
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")

// using CSIDL
#include <Shellapi.h>
#include <Shlobj.h> 
#pragma comment(lib, "Shell32.lib")

// listview & treeview
#include <commctrl.h>
#pragma comment(lib, "Comctl32.lib")
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

#define MAX_LOADSTRING 100
#define MAX_FILE_NAME 260
#define MAX_PATH_LEN 10240
#define WM_TASKBAR 1234
#define WM_TYPING 4321

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

struct ExeFile
{
	TCHAR name[MAX_FILE_NAME];
	TCHAR path[MAX_PATH_LEN];
	int preIndex;
};
std::vector<ExeFile> List;

typedef struct stringTree * node;
struct stringTree
{
	node child[27];		//26 kí tự a-z và kí tự đặc biệt
	BOOL isEndOfString;
	int index;
};
node root = NULL;

HWND hDlg;
HHOOK hHook = NULL;
NOTIFYICONDATA nfd;
UINT foundNum = 0;
TCHAR ProgramFilesPath[MAX_PATH_LEN], ProgramFiles86Path[MAX_PATH_LEN];

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

INT_PTR CALLBACK	DialogProc(HWND, UINT, WPARAM, LPARAM);

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
void OnPaint(HWND hWnd);
void OnDestroy(HWND hWnd);

void CreateTrayIcon(HWND hWnd, NOTIFYICONDATA &nfd);
void ShowContextMenu(HWND hWnd);
void LoadData(LPCWSTR path);
void fileSave(std::vector<ExeFile> list, LPCWSTR fileName);
int tree_Find(LPCWSTR String);
int* tree_Suggest(LPCWSTR String);
void lowerCase(TCHAR* String);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.
	hInst = hInstance;

	INITCOMMONCONTROLSEX icex;
	icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
	icex.dwICC = ICC_LISTVIEW_CLASSES | ICC_TREEVIEW_CLASSES;
	InitCommonControlsEx(&icex);

	// Tạo dialog và Notify Icon
	hDlg = CreateDialog(hInstance, MAKEINTRESOURCE(IDD_OPENBOX), NULL, (DLGPROC)DialogProc);
	CreateTrayIcon(hDlg, nfd);

	// Lấy địa chỉ Program Files
	SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES, NULL, SHGFP_TYPE_CURRENT, ProgramFiles86Path);
	if (wcsstr(ProgramFiles86Path, L"(x86)") != NULL)
		StrNCpy(ProgramFilesPath, ProgramFiles86Path, wcslen(ProgramFiles86Path) - 5);
	else
		StrCpy(ProgramFilesPath, ProgramFiles86Path);

	// tạo root
	root = new stringTree;
	for (int i = 0; i <= 26; i++)
	{
		root->child[i] = NULL;
		root->isEndOfString = FALSE;
	}

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_QUICKLAUNCH, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    //// Perform application initialization:
    //if (!InitInstance (hInstance, nCmdShow))
    //{
    //    return FALSE;
    //}

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_QUICKLAUNCH));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
		if (IsDialogMessage(hDlg, &msg))
			continue;
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }
    return (int) msg.wParam;
}

ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_RUN));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_QUICKLAUNCH);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_RUN));

    return RegisterClassExW(&wcex);
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, SW_HIDE);
   UpdateWindow(hWnd);

   return TRUE;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
	case WM_TASKBAR:
		switch (lParam)
		{
		case WM_LBUTTONDBLCLK:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_OPENBOX), hWnd, DialogProc);
			//ShowWindow(hWnd, SW_RESTORE);
			break;
		case WM_RBUTTONDOWN:
			ShowContextMenu(hWnd);
			break;
		}
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

INT_PTR CALLBACK DialogProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	HWND hEditBox = GetDlgItem(hDlg, IDC_COMBO1);
	int fIndex;
	int* select = NULL;
	TCHAR buffer[MAX_FILE_NAME];
	int id = LOWORD(wParam);
	int notifycode = HIWORD(wParam);
	switch (message)
	{
	case WM_INITDIALOG:
		SHAutoComplete(hEditBox, SHACF_AUTOAPPEND_FORCE_OFF | SHACF_AUTOSUGGEST_FORCE_OFF | SHACF_FILESYSTEM);
		return (INT_PTR)TRUE;
	case WM_TASKBAR:
		switch (lParam)
		{
		case WM_LBUTTONDBLCLK:
			ShowWindow(hDlg, SW_RESTORE);
			SetFocus(hEditBox);
			break;
		case WM_RBUTTONDOWN:
			ShowContextMenu(hDlg);
			break;
		}
	case WM_COMMAND:
		switch (id)
		{
		case IDC_COMBO1:
			switch (notifycode)
			{
			case CBN_EDITUPDATE:
				//ComboBox_GetText(hEditBox, buffer, MAX_FILE_NAME);
				GetWindowText(hEditBox, buffer, MAX_FILE_NAME);		
				if (wcslen(buffer) > 0)
				{
					select = tree_Suggest(buffer);
					for (int i = 0; i < 5; i++)
						ComboBox_DeleteString(hEditBox, 0);
					for (int i = 0; i < 5; i++)
					{
						if (select[i] > -1)
							ComboBox_AddString(hEditBox, List[select[i]].name);
					}
					delete select;
					ComboBox_GetMinVisible(hEditBox);
					ComboBox_ShowDropdown(hEditBox, TRUE);
				}
				else
					ComboBox_ShowDropdown(hEditBox, FALSE);
				//ComboBox_AddString(hEditBox, L"Bandizip");
				//ComboBox_AddString(hEditBox, L"PotPlayerMini64");
				//ComboBox_AddString(hEditBox, L"Honeyview");
				break;
			}
			break;
		case IDOK:
			ComboBox_GetText(hEditBox, buffer, MAX_FILE_NAME);
			EndDialog(hDlg, TRUE);
			
			fIndex = tree_Find(buffer);
			if (fIndex != -1)
				ShellExecute(NULL, L"open", List[fIndex].path, NULL, NULL, SW_SHOWDEFAULT);
			else
				MessageBox(0, L"path not found", 0, MB_OK);
			EndDialog(hDlg, TRUE);
			break;
		case IDCANCEL:
			EndDialog(hDlg, FALSE);
			break;
		case IDP_LOAD:
			LoadData(ProgramFilesPath);
			LoadData(ProgramFiles86Path);
			//fileSave(List, L"Save.txt");
			MessageBox(0, L"Database has loaded", L"Done", MB_OK);
			break;
		case IDP_VIEW:
			MessageBox(0, L"not avalable", 0, MB_OK);
			break;
		case IDP_EXIT:
			Shell_NotifyIcon(NIM_DELETE, &nfd);
			PostQuitMessage(0);
			break;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

void CreateTrayIcon(HWND hWnd, NOTIFYICONDATA &nfd)
{
	nfd.cbSize = sizeof(NOTIFYICONDATA);
	nfd.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_RUN));
	nfd.hWnd = hWnd;
	StrCpy(nfd.szTip, _T("Quick Launch"));
	nfd.uCallbackMessage = WM_TASKBAR;
	nfd.uFlags = NIF_ICON | NIF_TIP | NIF_MESSAGE;
	nfd.uID = NULL;

	Shell_NotifyIcon(NIM_ADD, &nfd);
}

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct)
{
	//HWND hwnd = CreateDialog(hInst, MAKEINTRESOURCE(MY_DIALOG), NULL, (DLGPROC)DialogProc);
	CreateTrayIcon(hWnd, nfd);

	//lấy địa chỉ Program Files
	SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES, NULL, SHGFP_TYPE_CURRENT, ProgramFiles86Path);
	if (wcsstr(ProgramFiles86Path, L"(x86)") != NULL)
		StrNCpy(ProgramFilesPath, ProgramFiles86Path, wcslen(ProgramFiles86Path) - 5);
	else
		StrCpy(ProgramFilesPath, ProgramFiles86Path);

	return TRUE;
}

void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
	switch (id)
	{
	case IDM_ABOUT:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
		break;
	case IDM_EXIT:
		Shell_NotifyIcon(NIM_DELETE, &nfd);
		DestroyWindow(hWnd);
		break;
	case IDP_LOAD:
		LoadData(ProgramFilesPath);
		fileSave(List, L"Save.txt");
		MessageBox(0, L"Database has loaded", L"Done", MB_OK);
		break;
	case IDP_VIEW:
		MessageBox(0, L"...", NULL, MB_OK);
		break;
	case IDP_EXIT:
		Shell_NotifyIcon(NIM_DELETE, &nfd);
		DestroyWindow(hWnd);
		break;
	}
}

void OnPaint(HWND hWnd)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	// TODO: Add any drawing code that uses hdc here...
	EndPaint(hWnd, &ps);
}

void OnDestroy(HWND hWnd)
{
	Shell_NotifyIcon(NIM_DELETE, &nfd);
	PostQuitMessage(0);
}

void ShowContextMenu(HWND hWnd)
{
	HMENU hMenu;

	POINT pt;
	GetCursorPos(&pt);

	hMenu = CreatePopupMenu();
	InsertMenu(hMenu, 0, MF_STRING, IDP_LOAD, TEXT("Load Database"));
	InsertMenu(hMenu, 0, MF_STRING, IDP_VIEW, TEXT("View Statics"));
	InsertMenu(hMenu, 0, MF_STRING, IDP_EXIT, TEXT("Exit"));

	SetForegroundWindow(hWnd);
	TrackPopupMenu(hMenu, TPM_RIGHTBUTTON, pt.x, pt.y, 0, hWnd, NULL);
	PostMessage(hWnd, WM_NULL, 0, 0);
	DestroyMenu(hMenu);
}

node node_Add(node &current, TCHAR ch, BOOL isEndOfString, BOOL &isExist)
{
	isExist = TRUE;
	int diff = ch - L'a';
	if (diff >= 26 || diff < 0)
		diff = 26;
	if (current->child[diff] == NULL)
	{
		isExist = FALSE;
		node p = new stringTree;
		for (int j = 0; j <= 26; j++)
			p->child[j] = NULL;
		p->isEndOfString = isEndOfString;
		current->child[diff] = p;
	}
	return current->child[diff];
}

// thêm chuỗi vào cây tìm kiếm
int tree_Add(LPCWSTR filename, int index, BOOL &isExist)
{
	int k, len, lastIndex;
	node current = root;
	TCHAR lower[MAX_FILE_NAME];

	StrCpy(lower, filename);
	len = wcslen(lower);

	// Chuyển thành chữ thường
	for (unsigned int i = 0; i < len; i++)
	{
		if (lower[i] >= L'A' && lower[i] <= L'Z')
		{
			lower[i] = lower[i] + (L'a' - L'A');
		}
	}

	for (unsigned int i = 0; i < len - 1; i++)
	{
		current = node_Add(current, lower[i], FALSE, isExist);
	}
	current = node_Add(current, lower[len - 1], TRUE, isExist);
	if (isExist)
		lastIndex = current->index;
	else
		lastIndex = -1;
	current->index = index;
	return lastIndex;
}

void LoadData(LPCWSTR path)
{
	BOOL isExist;
	TCHAR buffer[MAX_PATH_LEN];
	StrCpy(buffer, path);
	StrCat(buffer, _T("\\*"));

	WIN32_FIND_DATA fd;
	BOOL bFound = TRUE;
	HANDLE hFile;
	TCHAR *filePath, *folderPath;

	hFile = FindFirstFileW(buffer, &fd);
	if (hFile == INVALID_HANDLE_VALUE)
	{
		bFound = FALSE;
		return;
	}
	while (bFound)
	{
		if ((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN)
		{
			if (((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != FILE_ATTRIBUTE_DIRECTORY) && ((fd.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) != FILE_ATTRIBUTE_SYSTEM))
			{
				int len = wcslen(fd.cFileName);
				if (fd.cFileName[len - 4] == L'.' && fd.cFileName[len - 3] == L'e' && fd.cFileName[len - 2] == L'x' && fd.cFileName[len - 1] == L'e')
				{
					filePath = new TCHAR[wcslen(path) + len + 2];
					StrCpy(filePath, path);
					StrCat(filePath, _T("\\"));
					StrCat(filePath, fd.cFileName);

					// thêm địa chỉ file .exe vào vector lưu trữ
					ExeFile newFile;
					StrNCpy(newFile.name, fd.cFileName, len - 3);
					StrCpy(newFile.path, filePath);


					// thêm chỉ số thứ tự vào cây tìm kiếm
					int lastIndex = tree_Add(newFile.name, foundNum, isExist);
					newFile.preIndex = lastIndex;

					List.push_back(newFile);
					foundNum++;

					delete[] filePath;
				}
			}
			else if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) && (StrCmp(fd.cFileName, _T(".")) != 0) && (StrCmp(fd.cFileName, _T("..")) != 0))
			{
				folderPath = new TCHAR[wcslen(path) + wcslen(fd.cFileName) + 2];
				StrCpy(folderPath, path);
				StrCat(folderPath, _T("\\"));
				StrCat(folderPath, fd.cFileName);
				LoadData(folderPath);

				delete[] folderPath;
			}
		}
		bFound = FindNextFileW(hFile, &fd);
	}
}

void fileSave(std::vector<ExeFile> list, LPCWSTR fileName)
{
	HANDLE File;
	WCHAR* buffer = new TCHAR[MAX_PATH_LEN];
	DWORD dwBytesWritten;
	File = CreateFile(fileName, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (File != INVALID_HANDLE_VALUE)
	{
		for (unsigned int i = 0; i < list.size(); i++)
		{
			StrCpy(buffer, list[i].name);
			StrCat(buffer, L"\t");
			StrCat(buffer, list[i].path);
			StrCat(buffer, L"\n");
			WriteFile(File, buffer, wcslen(buffer) * sizeof(WCHAR), &dwBytesWritten, NULL);
		}
		CloseHandle(File);
		delete[] buffer;
	}
}

int tree_Find(LPCWSTR String)
{
	int index = -1;
	node current = root;
	int len = wcslen(String);
	TCHAR str[MAX_FILE_NAME];
	int diff;

	StrCpy(str, String);
	lowerCase(str);

	for (int i = 0; i < len; i++)
	{
		diff = str[i] - L'a';
		if (diff >= 26 || diff < 0)
			diff = 26;
		if (current->child[diff] == NULL) {
			return -1;
		}
		else {
			current = current->child[diff];
		}
	}
	if (current->isEndOfString == FALSE) {
		return -1;
	}
	else
	{
		index = current->index;
		TCHAR buffer[MAX_FILE_NAME];
		while (index != -1)
		{
			StrCpy(buffer, List[index].name);
			lowerCase(buffer);
			if (StrCmp(str, buffer) != 0)
				index = List[index].preIndex;
			else
				break;
		}
	}
	return index;
}

void tree_find5index(node current, int* index, int &found)
{
	if (found >= 5)
		return;
	else if (current->isEndOfString == TRUE)
	{
		index[found] = current->index;
		found++;
		return;
	}
	else
	{
		for (int i = 0; i <= 26; i++)
		{
			if (current->child[i] != NULL)
				tree_find5index(current->child[i], index, found);
		}
		return;
	}
}

int* tree_Suggest(LPCWSTR String)
{
	int* index = new int[5];
	for (int i = 0; i < 5; i++)
		index[i] = -1;

	int found = 0;
	node current = root;
	int len = wcslen(String);
	TCHAR str[MAX_FILE_NAME];
	int diff;
	
	StrCpy(str, String);
	lowerCase(str);

	for (int i = 0; i < len; i++)
	{
		diff = str[i] - L'a';
		if (diff >= 26 || diff < 0)
			diff = 26;
		if (current->child[diff] == NULL) {
			return index;
		}
		else {
			current = current->child[diff];
		}
	}
	if (current->isEndOfString == FALSE) {
		tree_find5index(current, index, found);
	}
	else
	{
		int cIndex = current->index;
		TCHAR buffer[MAX_FILE_NAME];
		do
		{
			StrCpy(buffer, List[cIndex].name);
			lowerCase(buffer);
			if (StrCmp(str, buffer) == 0)
			{
				index[found] = cIndex;
				found++;
			}
			cIndex = List[cIndex].preIndex;
		} while (cIndex != -1 && found < 5);
	}
	return index;
}

// chuyển kỉ tự trong chuỗi thành chữ thường
void lowerCase(TCHAR* String)
{
	for (int i = 0; i < wcslen(String); i++)
	{
		if (String[i] >= L'A' && String[i] <= L'Z')
		{
			String[i] = String[i] + (L'a' - L'A');
		}
	}
}