## Thông tin cá nhân
Họ và tên: **Đặng Quốc Thái**
MSSV: **1512501**

## Các chức năng
* Khi mở chương trình, 1 icon được thêm vào notification area.
* Nhấn chuột phải vào notification icon sẽ hiện popup menu lựa chọn gồm Load Database, View Statics và Quit.
* Menu Load Database sẽ quét toàn bộ file .exe trong thư mục Program Files và Program Files (x86)
* Menu View Statics chưa xây dựng chức năng
* Menu Quit sẽ thoát chương trình
* Double click vào notification icon sẽ hiện ra 1 dialog để gõ tìm file .exe để khởi động
* Mỗi khi gõ chữ vào trong khung tìm kiếm, 1 danh sách ứng dụng sẽ hiện ra (tối đa 5)
* Người dùng có thể dùng phím mũi tên để lựa chọn ứng dụng
* Người dùng nhấn Enter để khởi động ứng dụng (có tên hợp lệ)

## Ghi chú
1. Chức năng View Statics chưa được xây dựng, do đó khi lựa chọn, chỉ có thông báo lỗi hiện lên
2. Chức năng Load Database phụ thuộc vào thư mục trên máy người dùng, 1 thông báo quét hoàn tất sẽ hiện lên sau khi lựa chọn chức năng

## Link Repository
<https://bitbucket.org/3ar0n/quicklaunch.git>

## Link Youtube
<https://youtu.be/uzdQ7zbGf4M>